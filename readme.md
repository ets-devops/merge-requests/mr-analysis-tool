**Description:**

Gitlab MR-Analysis tool generates a csv file with metrics about Merge Requests. You can specify all the configuration for the tool in the config.json file. 


**Installation**

run `pip install requirements.txt` from the folder where requirements.txt is located. 

**Usage:**

Specify the full path to your config.json file with the f flag.
You may also overwrite the creation and end date of a sprint for all projects at the same time with the -c and -e flags 

**Do not run the tool twice with the same config, it will append the data to the existing file**

**Example:** 

`main.py -f C:\Users\devops\mr-analysis-tool\config.json`
`main.py -f C:\Users\devops\mr-analysis-tool\config.json -c 2020-01-01 -e 2020-01-21`

**Config File Parameters**

These parameters are defined in the config.json file.

project_ids : comma separated list of gitlab project ids

ex : `"project_ids":[3472737,10582521]`

creation_date: the date of the first day of the first sprint of the first release cycle

ex : `"creation_date":"2020-06-01"`

release_end_date : the maximum creation date. Anything created after this date will always be ignored.

ex: `"release_end_date":"2020-06-03"`

output_folder: full path to where the results of this run will be stored. Folder must exist

ex: `"output_folder":"C:/temp/"`

days_per_sprint: The numbers of days in a sprint

ex: `"days_per_sprint":3`

sprints_per_release: the amount of sprints to run the tool for.

ex: `"sprints_per_release":2`

**Architecture:**

The current architecture of the software uses synchronous calls to the Gitlab API. The MR data is fetched sequentially.
This results in slow performance ( around 1 minute run time per 100 MR)
The Main class parses the config file and instantiates a CSVManager object who calls on Calculator to do the API requests.

**Future**

One short term goal is to change the Architecture to a asynchronous model to accelerate the tool.
We want to refactor the tool so that CSVManager is only responsible for writing the output csv file at the end of the asynchronous calls.
UML Sequence diagrams are available for both existing and planned refactor in the appropriate refactor issue.

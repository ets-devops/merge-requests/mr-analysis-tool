import getopt
import sys
from datetime import timedelta, datetime

from src.CsvManager import CsvManager
import json


def printUsage():
    """Prints the usage syntax of the app."""
    print('usage : (default) ')
    print('main.py -f <config_file.json>')
    print('usage: (overwrites config file for all projects) ')
    print('main.py -f <config_file.json> -c <creation_date> -e <end_date>')


class Main:
    project_id = []
    creation_date = ""
    end_date = ""
    output_folder = ""
    creation_date_overwrite = ""
    end_date_overwrite = ""
    gitlabAuth = ""
    gitlabBaseUrl = ""
    days_per_sprint = 0
    sprintCount = 0

    def start(self):
        print("Parsing launch options")

        try:
            [opts, args] = getopt.getopt(sys.argv[1:], "p:c:e:f:", ["projectid=", "creation=", "end=", "file="])
        except getopt.GetoptError:
            printUsage()
            sys.exit(2)
        for opt, arg in opts:
            if opt == '-f':
                self.read_config(arg)
            elif opt == '-p':
                Main.project_id = arg
            elif opt == '-c':
                Main.creation_date_overwrite = arg
            elif opt == '-e':
                Main.end_date_overwrite = arg

        if Main.creation_date_overwrite:
            Main.creation_date = Main.creation_date_overwrite
        if Main.end_date_overwrite:
            Main.end_date = Main.end_date_overwrite

        if not Main.output_folder:
            print("No output folder specified in the provided config file.")
            printUsage()
            exit(2)
        if not Main.project_id:
            print("No project IDs found.")
            printUsage()
            exit(2)
        if not Main.gitlabAuth:
            print("No gitlab auth token provided in the config file")
            exit(2)

        print("Executing MR-Analysis Tool")

        output = CsvManager()
        for pid in Main.project_id:
            sprint_begin_date = Main.creation_date
            sprint_end_date = datetime.strftime(datetime.strptime(sprint_begin_date, "%Y-%m-%d")
                                                + timedelta(days=Main.days_per_sprint), "%Y-%m-%d")

            delta = timedelta(days=Main.days_per_sprint)
            creation_date_datetime = datetime.strptime(sprint_begin_date, "%Y-%m-%d")
            end_date_datetime = datetime.strptime(sprint_end_date, "%Y-%m-%d")
            sprint_begin_date = datetime.strftime(creation_date_datetime, "%Y-%m-%d")
            sprint_end_date = datetime.strftime(end_date_datetime, "%Y-%m-%d")
            for sprint in range(Main.sprintCount):

                if sprint_end_date > Main.end_date:
                    sprint_end_date = Main.end_date
                    output.generateCsv(pid, sprint_begin_date, sprint_end_date, Main.output_folder, Main.gitlabAuth,
                                       Main.gitlabBaseUrl,
                                       sprint, Main.days_per_sprint)
                    break
                output.generateCsv(pid, sprint_begin_date, sprint_end_date, Main.output_folder, Main.gitlabAuth,
                                   Main.gitlabBaseUrl,
                                   sprint, Main.days_per_sprint)
                sprint_begin_date = datetime.strftime(datetime.strptime(sprint_begin_date, "%Y-%m-%d") + delta,
                                                      "%Y-%m-%d")
                sprint_end_date = datetime.strftime(datetime.strptime(sprint_end_date, "%Y-%m-%d") + delta, "%Y-%m-%d")

    def read_config(self, arg):
        with open(arg) as json_config_file:
            config_data = json.load(json_config_file)
            Main.project_id = config_data['parameters']['project_ids']
            Main.creation_date = config_data['parameters']['creation_date']
            Main.end_date = config_data['parameters']['release_end_date']
            Main.output_folder = config_data['parameters']['output_folder']
            Main.days_per_sprint = config_data['parameters']['days_per_sprint']
            Main.sprintCount = config_data['parameters']['sprints_per_release']
            Main.gitlabAuth = config_data['gitlab']['auth_token']
            Main.gitlabBaseUrl = config_data['gitlab']['base_url']
        return True


if __name__ == '__main__':
    main = Main()
    main.start()

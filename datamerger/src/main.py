import json
import sys
import csv
import logging
import pandas as pd
import seaborn as sns
import os
import numpy as np
import matplotlib.pyplot as plt
from src.datamerger import DataMerger


class Main:
    extraction_file = None
    recurrency_file = None
    project_ID = None
    output_file = None
    sns.set_theme(style="darkgrid", context="notebook")

    @staticmethod
    def main():
        # Setup Logging
        logging.basicConfig(level=logging.DEBUG, filename='app.log', filemode='w',
                            format='%(name)s - %(levelname)s - %(message)s')
        logging.getLogger().addHandler(logging.StreamHandler(sys.stdout))

        if len(sys.argv) != 2:
            logging.critical("expected path to config file as the only argument.")
            logging.critical("usage example :  python main.py c:/path/config.json")
            exit(1)

        # Load config file
        config_loaded = Main.read_config(sys.argv[1])
        if not config_loaded:
            logging.critical("invalid information in the config file")
            exit(1)

        # Begin Main
        logging.debug("Begin Main")

        # Setup result file and csvwriter
        resultFilenameString = str(Main.output_file).replace('.csv', ("_" + str(Main.project_ID) + ".csv"))
        result_file = open(resultFilenameString, 'w+', newline='')
        # csv_writer = csv.writer(result_file)
        logging.debug("Calling DataMerger")
        # Call the DataMerger
        dm = DataMerger(Main.extraction_file, Main.recurrency_file, Main.project_ID)

        # output file
        # csv_writer = csv.writer(result_file)
        # csv_writer.writerows(dm)
        df = dm.execute()
        df.to_csv(resultFilenameString, index=False, header=True)

    @staticmethod
    def read_config(arg):

        with open(arg) as json_config_file:
            config_data = json.load(json_config_file)
            Main.extraction_file = config_data['parameters']['data_extraction_file']
            Main.recurrency_file = config_data['parameters']['recurrency_file']
            Main.project_ID = config_data['parameters']['projectID']
            Main.output_file = config_data['parameters']['output_file']

            # Quick Validation
            if not Main.extraction_file or not Main.recurrency_file or not Main.project_ID or not Main.output_file:
                logging.error("invalid parameter in config file")
                return False
            if not os.path.exists(str(Main.extraction_file)):
                logging.error("Path to extraction data doesn't exist : " + Main.extraction_file)
                return False
            if not os.path.exists(str(Main.recurrency_file)):
                logging.error("Path to recurrency data doesn't exist :" + Main.recurrency_file)
                return False
        return True


if __name__ == "__main__":
    Main.main()

import logging
import os
import pandas as pd
from numpy import int64


class DataMerger:
    def __init__(self, extraction_file, recurrency_file, project_ID):
        self.extraction_file = extraction_file
        self.recurrency_file = recurrency_file
        self.project_ID = project_ID

    def execute(self):
        try:
            # Load Extraction File
            logging.debug("Extraction file found : " + self.extraction_file)
            extraction_filename = os.path.join(str(self.extraction_file))
            extraction_df = pd.read_csv(extraction_filename)

            # Load Recurrency File
            logging.debug("Reccurency file found : " + self.recurrency_file)
            filename = os.path.join(str(self.recurrency_file))
            recurrency_df = pd.read_csv(filename)

            # Get only the right project
            extraction_project_df = extraction_df[extraction_df["Project ID"] == int(self.project_ID)]
            print(extraction_project_df.head())

            # Save Size before modifications
            extraction_size = len(extraction_df["Project ID"])
            logging.info("Extraction tool rows : " + str(extraction_size))
            recurrency_size = len(recurrency_df["Project ID"])
            logging.info("Reccurency tool rows : " + str(recurrency_size))
            project_size = len(extraction_project_df)
            logging.info("Matching Rows for Project ID before merge: " + str(extraction_project_df))

            # Merge based on MRID
            extraction_project_df = pd.merge(extraction_project_df, recurrency_df, how='left', on="MR ID")
            merged_project_size = len(extraction_project_df)
            logging.info("Matching Rows for Project ID after merge: " + str(extraction_project_df))

            # fix float for last column
            extraction_project_df['max count'] = extraction_project_df['max count'].apply(lambda x: int(x) if x == x else "")
            logging.info("Rows after null cleanup : " + str(len(extraction_project_df)))

            # Only select rows with a max_count
            extraction_project_df = extraction_project_df[extraction_project_df['max count'] != ""]
            logging.info("Rows after usable selection : " + str(len(extraction_project_df)))

            return extraction_project_df
        except Exception as e:
            logging.exception("Program crashed. Error : %s", e)

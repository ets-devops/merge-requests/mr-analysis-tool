from _datetime import datetime
from datetime import timedelta
import requests
import pytz


class Calculator:
    # TODO : Some strings dont need to be variables

    str_mr = "/merge_requests"
    str_commits = "/commits"
    str_created_at = "created_at"
    str_created_after = "created_after"
    str_created_before = "created_before"
    str_discussions = "/discussions"
    str_notes = "/notes"
    headers = {}
    str_pagination_size = "per_page=100"

    list_mrs_lead_times = []
    list_mrs_ids = []
    reviewer_list = []
    trueReviewerList = []
    creation_time = ""
    j = 0
    projectRunId = 0
    mergeRequest_ID = 0
    MR_dictionary = {}
    commenterCount = 0
    commitersCount = 0
    committers = {}
    cache_mr_notes = {}
    cache_mr_commit = {}
    authorDictionary = {}
    cache_mr_discussion = {}

    def __init__(self, token, baseurl, sprintCount, days_per_sprint):
        Calculator.headers = {'Private-token': token}
        self.fixed_url = baseurl
        self.sprintBeginDate = None
        self.sprint = sprintCount + 1
        self.days_per_sprint = days_per_sprint

    def getSprint(self):
        """
        Gets the sprint number for this MR
        :return: sprint
        """
        return self.sprint

    def time_standardizer(self, input_time_date):
        """
        casts a ISO 8601 time string into a datetime object
        :param input_time_date:  String iso8601_time
        :return: datetime
        """
        datestring = str(input_time_date).replace("T", "  ").replace("Z", "")
        if datestring != "None":
            # datestring = datetime.replace(datestring, microsecond=0)
            try:
                time_date_standard_format = datetime.strptime(datestring, '%Y-%m-%d  %H:%M:%S')
            except:
                time_date_standard_format = datetime.strptime(datestring, '%Y-%m-%d  %H:%M:%S.%f')
        else:
            time_date_standard_format = "None"
        return time_date_standard_format

    def getMRID(self):
        """
        Gets the MR ID of this MR
        :return: MRID
        """
        return self.mergeRequest_ID

    def getCreationDate(self):
        """
        Gets the Creation Date of this MR
        stores the API request in self.general_details
        :return: Creation Date
        """
        # Keep as single request example
        # TODO: change this implementation to cached strategy for performance.
        # using 1 request per mr @ 0.1 second per MR
        result = requests.get(self.fixed_url + "/api/v4/projects/" +
                              str(self.projectRunId) + "/merge_requests/" + str(self.mergeRequest_ID),
                              headers=self.headers).json()
        creation_date = result["created_at"]
        # I keep the response to the API call to utilize later in fileCount functions
        self.general_details = result
        creation_localDateTimeStr = self.localTimeConverter(creation_date)
        return creation_localDateTimeStr

    def getEndDate(self):
        """
        gets the enddate of the MR using the MR_dictionary
        populated by getMRIDS
        :return: end_date
        """
        mr = self.MR_dictionary[self.mergeRequest_ID]
        if mr["state"] == "closed":
            end_date = str(mr["closed_at"])
            end_localDateTimeStr = self.localTimeConverter(end_date)
        elif mr["state"] == "merged":
            end_date = str(mr["merged_at"])
            end_localDateTimeStr = self.localTimeConverter(end_date)
        elif mr["state"] == "opened":
            end_localDateTimeStr = str("Open")
        else:
            end_localDateTimeStr = str("Unknown")
        return end_localDateTimeStr

    def getLeadTime(self):
        """
        returns the lead time in minutes or "open" if the MR is open
        uses the MR_dictionary
        """
        mr = self.MR_dictionary[self.mergeRequest_ID]
        if (mr["state"] != "opened") & (self.getEndDate() != "None"):
            leadTime = self.time_standardizer(self.getEndDate()) - self.time_standardizer(self.getCreationDate())
            leadTime = self.to_minutes(leadTime)
        else:
            leadTime = "open"
        return leadTime

    def getActivityCount(self):
        """
        returns number of discussions using the cached discussion API request
        :return: int
        """
        # result = requests.get("https://gitlab.com/api/v4/projects/" +
        #                       str(self.projectRunId) + "/merge_requests/" + str(self.mergeRequest_ID) +
        #                       "/discussions" + "?" + self.str_pagination_size,
        #                       headers=self.headers).json()
        result = self.cache_mr_discussion
        activityCount = len(result)
        return activityCount

    def getFirstCommentTime(self):
        """
        returns either "No comment" or time to first comment in minutes
        using the cached mr_notes from API request
        :return: string
        """
        i = 0
        amountFirstCommentTime = ""
        # result = requests.get("https://gitlab.com/api/v4/projects/" +
        #                       str(self.projectRunId) + "/merge_requests/" + str(self.mergeRequest_ID) +
        #                       "/notes" + "?" + self.str_pagination_size, headers=self.headers).json()
        result = self.cache_mr_notes
        while i < len(result):
            if str(result[i]["system"]) == "False" and result[i]["author"]["name"] != self.general_details["author"][
                "name"]:
                firstCommentTime = result[i]["created_at"]
                firstCommentTime = datetime.strftime(self.localTimeConverter(firstCommentTime),
                                                     ("%Y-%m-%dT%H:%M:%S.%fZ"))
                amountFirstCommentTime = self.time_standardizer(firstCommentTime) - self.time_standardizer(
                    self.getCreationDate())
                amountFirstCommentTime = self.to_minutes(amountFirstCommentTime)
                break
            else:
                amountFirstCommentTime = "No Comment"
            i += 1
        return amountFirstCommentTime

    def getCommitCount(self):
        """
        returns the number of commits
        using the cached mr_commit API request
        :return:
        """
        # result = requests.get("https://gitlab.com/api/v4/projects/" +
        #                       str(self.projectRunId) + "/merge_requests/" + str(
        #     self.mergeRequest_ID) + "/commits" + "?" + self.str_pagination_size,
        #                       headers=self.headers).json()

        result = self.cache_mr_commit
        self.commitCount = len(result)
        return self.commitCount

    def getMeanCommitTime(self):
        """
        Calculates the mean time between commits
        requires a previous call to getCommitCount
        returns "open" if MR is open or mean commit time
        """
        if self.commitCount == 0:
            return "no commits"
        leadTime = self.getLeadTime()
        if leadTime != "open":
            commitMeanTime = leadTime / self.commitCount
        else:
            commitMeanTime = "open"
        self.commitCount = 0
        return commitMeanTime

    def getMeanReviewTime(self):
        """
        calculates the mean review time between reviews inside a single MR
        assuming the notes go in order : Dev-Reviewer-Dev-Reviewer...
        returns that mean or the lead time if no discussions
        :return:
        """
        # get all commit-system:false and average them
        # https://gitlab.com/api/v4/projects/3472737/merge_requests/1573/notes?sort=asc
        # results = requests.get("https://gitlab.com/api/v4/projects/" +
        #                        str(self.projectRunId) + "/merge_requests/" + str(self.mergeRequest_ID) +
        #                        "/notes" + "?" + self.str_pagination_size + "&sort=asc", headers=self.headers).json()
        results = self.cache_mr_notes
        if len(results) < 1:
            return "No Notes"
        beginTime = results[0]["created_at"]
        reviewCount = 0
        reviewTotalTime = timedelta(microseconds=0)
        awaitingReview = True
        for activity in results:
            if activity["system"] == False and awaitingReview == True:
                endTime = activity["created_at"]
                timeDelta = self.time_standardizer(endTime) - self.time_standardizer(beginTime)
                reviewTotalTime = reviewTotalTime + timeDelta
                reviewCount = reviewCount + 1
                awaitingReview = False
            else:
                beginTime = activity["created_at"]
                awaitingReview = True
        if reviewCount > 0:
            reviewMeanTime = reviewTotalTime / reviewCount
            reviewMeanTime = self.to_minutes(reviewMeanTime)
        else:
            reviewMeanTime = self.getLeadTime()
        return reviewMeanTime

    def getCommenterCount(self):
        """
        returns the commenters count using the cached authorDictionary
        :return: int
        """
        return len(self.authorDictionary)

    def getCommenter(self):
        """
        returns a comma separated string containing the names of the commenters
        :return: string
        """

        cList = []
        for k in self.authorDictionary.keys():
            cList.append(k)
            cList.append(',')

        commenters = ''.join(cList)
        # remove extra comma at the end
        commenters = commenters[:-1]
        return commenters

    def buildAuthorDict(self):
        """
        build a dictionnary of discussion authors using the cached mr_notes API request
        this method is called in executeMRrequest
        key:  discussion author
        value: occurence of that author in the discussions
        :return: builds a dictionnary of authors[occurence]
        """
        self.authorDictionary.clear()
        for activity in self.cache_mr_notes:
            if activity["system"] is False:
                author = activity["author"]["username"]
                if author in self.authorDictionary:
                    self.authorDictionary[author] = self.authorDictionary[author] + 1
                else:
                    self.authorDictionary[author] = 1

    def getCommitersCount(self):
        """
        uses the commiters dictionnary to return the amount of commiters
        """
        return len(self.committers)

    def getCommiters(self):
        """
        uses the commiters dictionnary to return a comma separated string of commiters for this MR
        """
        # https://gitlab.com/api/v4/projects/3472737/merge_requests/3/commits
        commitersList = []
        for k in self.committers.keys():
            commitersList.append(k)
            commitersList.append(',')
        commiterString = ''.join(commitersList)
        commiterString = commiterString[:-1]
        return commiterString

    def buildCommitersDict(self):
        """
        build a dictionnary of commiters using the cached mr_commit API request
        this method is called in executeMRrequest
        key:  commiter
        value: occurence of that committer in the commits
        :return: builds a dictionnary of commiter[occurence]
        """
        for commit in self.cache_mr_commit:
            commiter = commit["author_name"]
            if commiter in self.committers:
                self.committers[commiter] = self.committers[commiter] + 1
            else:
                self.committers[commiter] = 1

    def getInitialFileCount(self):
        """
        returns the number of files from the first commit in the MR
        :return:
        """
        # find first commit
        commitcount = len(self.cache_mr_commit)
        if commitcount < 1:
            return 0
        earliest_commit = self.cache_mr_commit[commitcount - 1]
        sha = earliest_commit["short_id"]

        # from the mr
        # find files for that commit.
        # https://gitlab.com/api/v4/projects/19815165/repository/commits/a02ebff1/diff
        commit_diff = requests.get(self.fixed_url + "/api/v4/projects/" +
                                   str(self.projectRunId) + "/repository/commits/" + str(sha) +
                                   "/diff?&per_page=100", headers=self.headers).json()
        files = len(commit_diff)
        return files

    def getFinalFileCount(self):
        """
        gets the number of files in the MR
        :return:
        """
        files = self.general_details["changes_count"]
        return files

    def getState(self):
        """
        returns the state of the MR as a string using the MR_dictionnary built in getMRID
        """
        state = self.MR_dictionary[self.mergeRequest_ID]["state"]
        return state

    def getInitialFiles(self):
        """
        returns names of initial files
        :return:
        """
        files = "None"
        return files

    def getFinalFiles(self):
        """
        return names of final files
        """
        files = "None"
        return files

    def getTrueReviewer(self):
        """
        gets the most popular reviewer based off discussion, excluding the author of the MR
        :return:
        """
        i = 0
        commenters_list = []
        # result = requests.get("https://gitlab.com/api/v4/projects/" +
        #                       str(self.projectRunId) + "/merge_requests/" + str(self.mergeRequest_ID) +
        #                       "/notes" + "?" + self.str_pagination_size, headers=self.headers).json()

        result = self.cache_mr_notes
        while i < len(result):
            if str(result[i]["system"]) == "False":
                commenters_list.append(result[i]["author"]["name"])
            i += 1
        if commenters_list:
            trueReviewer = self.most_frequent(entryList=commenters_list, number_on_top=2)
            if trueReviewer[0] == self.general_details["author"]["name"]:
                if len(trueReviewer) > 1:
                    trueReviewer = trueReviewer[1]
                else:
                    trueReviewer = "No commenter"
            else:
                trueReviewer = trueReviewer[0]
        else:
            trueReviewer = "No commenter"
        self.trueReviewerList.append(trueReviewer)
        return trueReviewer

    def getAssignedReviewer(self):
        """
        returns the assigned reviewer using general_details
        populated in getCreationDate
        :return:
        """
        if self.general_details["assignee"]:
            reviewer = self.general_details["assignee"]["name"]
        else:
            reviewer = "unassigned"
        return reviewer

    def ExecuteMR(self, MRID):
        """
        Calls all API requests for a specified MRID
        Populates dictionnaries
        Populates Cached API requests

        :param MRID: The MR ID we are requesting data from
        :return: a table with all the metrics values for this MR ID
        """
        # TODO make all API Calls here and store results in class attribute.
        self.cache_mr_notes = requests.get(self.fixed_url + "/api/v4/projects/" +
                                           str(self.projectRunId) + "/merge_requests/" + str(self.mergeRequest_ID) +
                                           "/notes" + "?" + self.str_pagination_size + "&sort=asc",
                                           headers=self.headers).json()
        self.cache_mr_commit = requests.get(self.fixed_url + "/api/v4/projects/" +
                                            str(self.projectRunId) + "/merge_requests/" + str(self.mergeRequest_ID) +
                                            "/commits" + "?" + self.str_pagination_size, headers=self.headers).json()
        self.cache_mr_discussion = requests.get(self.fixed_url + "/api/v4/projects/" +
                                                str(self.projectRunId) + "/merge_requests/" + str(
            self.mergeRequest_ID) +
                                                "/discussions" + "?" + self.str_pagination_size,
                                                headers=self.headers).json()

        # Build required dictionary from results.
        self.buildCommitersDict()
        self.buildAuthorDict()

        MR = [str(self.getProjectID()), MRID, str(self.getCreationDate()), str(self.getSprintDay()),
              str(self.getState()), str(self.getEndDate()),
              str(self.getSprintEndDay()),
              str(self.getLeadTime()),
              str(self.getActivityCount()),
              str(self.getFirstCommentTime()), str(self.getCommitCount()), str(self.getMeanCommitTime()),
              str(self.getMeanReviewTime()),
              str(self.getCommenterCount()), str(self.getCommenter()), str(self.getCommitersCount()),
              str(self.getCommiters()),
              str(self.getInitialFileCount()), str(self.getFinalFileCount()), str(self.getInitialFiles()),
              str(self.getFinalFiles()),
              str(self.getTrueReviewer()), str(self.getAssignedReviewer()), str(self.getMRSize(MRID)),
              str(self.getSprint()), str(self.getReworkCommits())]

        # Clear cache for next run
        self.cache_mr_notes.clear()
        self.cache_mr_commit.clear()
        self.committers.clear()
        self.authorDictionary.clear()
        self.cache_mr_discussion.clear()
        return MR

    def ExecuteAllMR(self, projectID, beginDate, endDate):
        """
        Calls ExecuteMR for all MRs. Returns a table
        with all values for all MRs.
        :param projectID: Gitlab project ID
        :param beginDate: minimum MR creation date
        :param endDate: maximum MR creation date
        :return:
        """
        self.projectRunId = projectID  # stored for API request creation..
        self.sprintBeginDate = beginDate
        MRIDlist = self.getMRIDs(projectID, beginDate, endDate)
        result = []
        for mrid in MRIDlist:
            self.mergeRequest_ID = mrid
            row = self.ExecuteMR(mrid)
            result.append(row)
        return result

    def getMRIDs(self, projectID, beginDate, endDate):
        """
        returns a list of MR IDs between the specified dates.
        populates MR_dictionary
        :param projectID: gitlab project ID
        :param beginDate: minimum MR creation date
        :param endDate:  maximum MR creation date
        :return:
        """
        MRIDList = []
        resultObject = requests.get(self.fixed_url + "/api/v4/projects/" + str(projectID) + "/merge_requests?"
                                    + "pagination=keyset&created_after="
                                    + str(beginDate) + "&created_before=" + str(endDate) + "&per_page=100",
                                    headers=self.headers)
        result = resultObject.json()
        # for gitlab version < 14.0, otherwise use resultObject.next ?
        if resultObject.links:
            if 'next' in resultObject.links:
                # has another page, lets cycle them
                nextpageURL = resultObject.links['next']['url']
                while nextpageURL:
                    nextpageResponse = requests.get(nextpageURL, headers=self.headers)
                    result.extend(nextpageResponse.json())
                    if nextpageResponse.links:
                        if 'next' in nextpageResponse.links:
                            nextpageURL = nextpageResponse.links['next']['url']
                        else:
                            nextpageURL = None
                    else:
                        nextpageURL = None

        for mr in result:
            # build the MRIDList
            MRIDList.append(mr["iid"])
            # Cache results for optimisation later
            self.MR_dictionary[mr["iid"]] = mr
        return MRIDList

    def most_frequent(self, entryList, number_on_top):
        """
        returns an ordered list of most frequent reviewer
        """
        reviewer_counter = {}
        for reviewer in entryList:
            if reviewer in reviewer_counter:
                reviewer_counter[reviewer] += 1
            else:
                reviewer_counter[reviewer] = 1
        popular_reviewers = sorted(reviewer_counter, key=reviewer_counter.get, reverse=True)
        if len(popular_reviewers) >= number_on_top:
            top_n = popular_reviewers[:number_on_top]
        else:
            top_n = popular_reviewers[:len(popular_reviewers)]
        return top_n

    def getSprintDay(self):
        """
        calculates and returns the day number inside a sprint
        based on sprint start date.
        :return:
        """
        currentDateTimeStr_UTC = self.general_details['created_at']
        sprintStartDate = datetime.strptime(self.sprintBeginDate, '%Y-%m-%d')
        current_LocalDateTime = self.localTimeConverter(currentDateTimeStr_UTC)
        elapsedTime = current_LocalDateTime - sprintStartDate
        elapsedHours = int(elapsedTime.total_seconds()) / 3600
        elapsedDays = int(elapsedHours) / 24
        sprintDay = int(elapsedDays) + 1
        return sprintDay

    def getMRSize(self, MRID):
        mr_size = float(0)
        # Do a request (example: )
        # https: // gitlab.com / api / v4 / projects / 21860002 / merge_requests / 13 / changes
        result = requests.get(
            self.fixed_url + "/api/v4/projects/" + str(self.projectRunId) + "/merge_requests/" + str(
                MRID) + "/changes",
            headers=self.headers).json()
        # add the numbers for each chunk
        for chunk in result['changes']:
            diff_string = str(chunk['diff'])
            # check if the string has the diff size
            if diff_string.find("@@") != -1:
                diff_string = diff_string.split('@@')[1].split('@@')[0]
                # this gives us the format : @@ -1,10 +1,5 @@ or @@ -1 +1 @@
                deletions_count = float(0)
                additions_count = float(0)
                # extract deletions
                del_index = diff_string.find('-')
                add_index = diff_string.find('+')
                if del_index != -1:
                    del_line_index = diff_string.find(',', del_index, add_index)
                    if del_line_index != -1:
                        del_line_index += len(',')
                        del_end_index = diff_string.find(' ', del_line_index)
                        deletions_count = diff_string[del_line_index:del_end_index]
                    else:
                        # its a one line situation of type @@ -1 +1 @@
                        deletions_count = 1

                # extract additions
                if add_index != -1:
                    add_line_index = diff_string.find(',', add_index)
                    if add_line_index != -1:
                        add_line_index += len(',')
                        add_end_index = diff_string.find(' ', add_line_index)
                        additions_count = diff_string[add_line_index:add_end_index]
                    else:
                        # its a one line situation of type @@ -1 +1 @@
                        additions_count = 1
                # Chunk Size
                chunk_size = float(deletions_count) + float(additions_count)
                # Add chunk size to MR size
                mr_size = mr_size + chunk_size
            # add the chunks together
            else:
                mr_size += 0.0  # do nothing
        return mr_size

    def to_minutes(self, time):
        return time.total_seconds() / 60

    def getProjectID(self):
        return self.projectRunId

    def localTimeConverter(self, date_time):
        """
        this method gets the datetime in UTC (Zulu/Greenwich Mean Time (GMT) time zone), converts and returns datetime
        string in the Montreal local time zone.
        # TODO : local time zone can be added to config.json to avoid hard coding inside the method.
        """
        localFormat = "%Y-%m-%dT%H:%M:%S.%fZ"
        try:
            datetime_UTC = datetime.strptime(date_time, localFormat)
            datetime_UTC = datetime.replace(datetime_UTC, microsecond=0, tzinfo=pytz.utc)
            local_time = datetime_UTC.astimezone(pytz.timezone("America/Montreal"))
            localFormat = "%Y-%m-%d %H:%M:%S.%f"
            date_localTime = local_time.strftime(localFormat)
        except:
            date_localTime = date_time
        if date_localTime != "None":
            date_localTime_dateTimeObject = datetime.strptime(date_localTime, '%Y-%m-%d %H:%M:%S.%f')
            date_localTime_dateTimeObject = datetime.replace(date_localTime_dateTimeObject, microsecond=0)
        else:
            date_localTime_dateTimeObject = "None"
        return date_localTime_dateTimeObject

    def getSprintEndDay(self):
        """
        calculates and returns the day number inside a sprint
        based on sprint end date.
        :return: "None" or int: end sprint day
        """

        endDate = self.getEndDate()
        if endDate == "Open" or endDate == "Unknown" or endDate == "None":
            return "None"
        sprintStartDate = datetime.strptime(self.sprintBeginDate, '%Y-%m-%d')
        current_LocalDateTime = endDate
        elapsedTime = current_LocalDateTime - sprintStartDate
        elapsedHours = int(elapsedTime.total_seconds()) / 3600
        elapsedDays = int(elapsedHours) / 24
        sprintDay = int(elapsedDays) + 1
        sprintDay = sprintDay % self.days_per_sprint
        if sprintDay == 0:
            sprintDay = self.days_per_sprint
        return sprintDay

    def getReworkCommits(self):
        """returns the amount of commits within a MR that are created after the MR creation date"""
        commits = self.cache_mr_commit
        MRCreationDate = self.general_details["created_at"]
        reworkCommits = 0
        for commit in commits:
            commitDate = commit["created_at"]
            if commitDate > MRCreationDate:
                reworkCommits = reworkCommits + 1
        return reworkCommits

import os
import src.calculator
import csv


class CsvManager:
    """this class handles outputs to csv files"""

    def generateCsv(self, project_id, creation_date, end_date, output_folder, gitlabtoken, baseurl, sprintCount,days_per_sprint):
        # Generate results
        calc = src.calculator.Calculator(gitlabtoken, baseurl, sprintCount, days_per_sprint)
        result = calc.ExecuteAllMR(project_id, creation_date, end_date)
        labels = ["Project ID", "MR ID", "Creation Date", "Sprint Day", "State", "End Date","End Sprint Day", "Lead Time", "#Discussions",
                  "Time to First Comment",
                  "# Commits", " Mean Time between commits", "Mean Time between review", "#commenters", "Commenters",
                  "# Commiters"
            , "Commiters", "# Initial Files", "# Final Files", "Initial Files", "Final files",
                  "True Reviewer", "Assigned Reviewer", "MR Size", "Sprint", "Rework Commits" ]
        filename = "results.csv"
        fullname = os.path.join(output_folder, filename)
        with open(fullname, "a+", newline='',
                  encoding="utf-8") as new_file:
            csv_writer = csv.writer(new_file, delimiter=',')
            if not self.isNonZeroFile(fullname):
                # write headers
                csv_writer.writerow(labels)
            # write data
            for line in result:
                csv_writer.writerow(line)
        return True

    def isNonZeroFile(self, fpath):
        return os.path.isfile(fpath) and os.path.getsize(fpath) > 0

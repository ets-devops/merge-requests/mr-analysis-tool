import unittest
import csv
import sys
import os
from main import Main

def setUpModule():
    print(" ")
    print("------------------------")
    print("BEGIN MR-ANALYSIS TESTS ")
    print("------------------------")
    print(" ")
    Main.read_config(Main,r"config.json")
    token = os.environ["CICDAPITOKEN"]
    Main.gitlabAuth = token

class TestStringMethods(unittest.TestCase):

    def test_mainLoaded(self):  
        Main.start(Main)
        self.assertEqual(Main.project_id,[3472737,10582521], "config file error. expected project_id : [10582521]")
        self.assertEqual(Main.creation_date,"2020-06-01", "config file error. expected creation date : 2020-06-01")
        self.assertEqual(Main.days_per_sprint,1, "config file error. expected days_per_sprint : 1")
        self.assertEqual(Main.sprintCount,1, "config file error. expected sprints_per_release : 1")
        file = open(r"./results/results.csv")
        reader = csv.reader(file)
        lines= len(list(reader))
        file.close()
        self.assertEqual(lines, 10, "results.csv has wrong number of results")


if __name__ == '__main__':
    unittest.main()

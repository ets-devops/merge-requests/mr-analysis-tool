import json
import sys
import csv
import logging

from recurrency import Recurrency


class Main:
    project_id = None
    days = None
    output_folder = None
    target_MRID = None
    target_MRID_begin = None
    target_MRID_end = None
    auth_token = None
    gitlabBaseUrl = None
    updated_before = None
    updated_after = None

    @staticmethod
    def main():

        logging.basicConfig(level=logging.DEBUG, filename='app.log', filemode='w',
                            format='%(name)s - %(levelname)s - %(message)s')

        if len(sys.argv) != 2:
            logging.critical("expected path to config file as the only argument.")
            logging.critical("usage example :  python main.py c:/path/results.csv")
            exit(1)

        config_loaded = Main.read_config(sys.argv[1])
        if not config_loaded:
            logging.critical("invalid information in the config file")
            exit(1)

        result_file = open(Main.output_folder + 'recurrency.csv', 'w', newline='')
        csv_writer = csv.writer(result_file)
        csv_writer.writerow(['MRID', 'Files changed', 'Project ID', 'days', 'updated_before', 'updated_after', 'merge_sha', 'squash_sha','sha','sha strategy', 'MRID range', 'max filenames' , 'max count', 'maxMRIDlist'])
        for MR in range(Main.target_MRID_begin, Main.target_MRID_end + 1):
            reccurency = Recurrency(Main.project_id, Main.days, Main.output_folder, MR,
                                    Main.auth_token, Main.gitlabBaseUrl, Main.updated_before, Main.updated_after)
            if reccurency.MRFilenames:
                csv_writer.writerow(
                    [MR, reccurency.MRFilenames, Main.project_id, Main.days, reccurency.update_before,
                     reccurency.update_after, reccurency.apiResult_merge_sha,reccurency.apiResult_squash_sha,
                     reccurency.apiResult_sha, reccurency.sha_strategy, reccurency.MRID_range, reccurency.maxFileName,
                     reccurency.maxFileCount, reccurency.maxFileMRList])

    @staticmethod
    def read_config(arg):
        with open(arg) as json_config_file:
            config_data = json.load(json_config_file)
            Main.project_id = config_data['parameters']['project_id']
            Main.days = config_data['parameters']['days']
            Main.output_folder = config_data['parameters']['output_folder']
            Main.target_MRID_begin = config_data['parameters']['target_MRID']['begin']
            Main.target_MRID_end = config_data['parameters']['target_MRID']['end']
            Main.auth_token = config_data['gitlab']['auth_token']
            Main.gitlabBaseUrl = config_data['gitlab']['base_url']
            Main.updated_before = config_data['parameters']['updated_before']
            Main.updated_after = config_data['parameters']['updated_after']

            # Quick Validation
            if not Main.project_id:
                logging.error("invalid project id in config file")
                return False
            if not Main.days:
                logging.error("invalid days in config file")
                return False
            if not Main.output_folder:
                logging.error("invalid in config file")
                return False
            if not Main.target_MRID_begin or not Main.target_MRID_end:
                logging.error("invalid MRID range in config file")
                return False
            if not Main.auth_token:
                logging.error("Invalid github auth token in config file")
                return False
            if not Main.gitlabBaseUrl:
                logging.error("invalid gitlab base url in config file")
                return False
            if not Main.updated_before or not Main.updated_after:
                logging.error("invalid updated date range in config file")
                return False

        return True


if __name__ == "__main__":
    Main.main()

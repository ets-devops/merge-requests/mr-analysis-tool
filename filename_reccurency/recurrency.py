import requests
import logging
from datetime import datetime
from datetime import timedelta


class Recurrency:
    def __init__(self, project_id, days, output_folder, target_MRID,
                 auth_token, gitlabBaseUrl, updated_before, updated_after):
        """
        Constructor initialises passed variables
        """

        self.gitlabBaseUrl = gitlabBaseUrl
        self.auth_token = auth_token
        self.target_MRID = target_MRID
        self.output_folder = output_folder
        self.days = days
        self.project_id = project_id
        self.headers = {'Private-token': auth_token}
        self.MRFilenames = {}  # This is the result object
        self.MRmergeDate = None
        self.update_before = updated_before
        self.update_after = updated_after
        self.sha_strategy = None
        self.apiResult_merge_sha = None
        self.apiResult_squash_sha = None
        self.apiResult_sha = None
        self.MRID_range = None
        self.maxFileCount = -1
        self.maxFileName = None
        self.filerecurrentMRID = {}  # this is the list of MRID in the future for a file.
        self.maxFileMRList = []

        try:
            self.execute()
        except Exception as e:
            logging.exception("Program crashed. Error : %s", e)

    def fetchMRInfo(self):
        """
        This method constructs and Executes the API call to get the merge commit SHA and dates related to a MR
        :return: the SHA of the commit associated with an MR
        """
        apiResult = requests.get(self.gitlabBaseUrl + "/api/v4/projects/" +
                                 str(self.project_id) + "/merge_requests/" + str(self.target_MRID),
                                 headers=self.headers).json()
        logging.info("--- logging fetchMRInfo() result : ---")
        logging.info(" ")
        logging.info(str(apiResult))
        logging.info("--- end of fetchMRINfo() result ---")
        logging.info("executing for MRID : " + str(apiResult["iid"]))

        self.apiResult_merge_sha = apiResult["merge_commit_sha"]
        self.apiResult_squash_sha = apiResult["squash_commit_sha"]
        self.apiResult_sha = apiResult["sha"]

        if apiResult["merge_commit_sha"] is not None:
            commitSha = apiResult["merge_commit_sha"]
            self.sha_strategy = "merge_sha"
            logging.info("using merge_commit_sha : " + str(commitSha))
        elif apiResult["squash_commit_sha"] is not None:
            commitSha = apiResult["squash_commit_sha"]
            self.sha_strategy = "squash_sha"
            logging.info("using squash_commit_sha : " + str(commitSha))
        else:
            commitSha = apiResult["sha"]
            self.sha_strategy = "sha"
            logging.info("sha: " + str(commitSha))

        self.MRmergeDate = apiResult["merged_at"]
        logging.info("using merged_at : " + str(self.MRmergeDate))
        if self.MRmergeDate is None:
            commitSha = None
        return commitSha

    def AddFilenames(self, commitSha):
        """
        This method adds the filenames associated to a commit SHA to the MRFilename dictionnary
        ex
        https://gitlab.com/api/v4/projects/21860002/repository/commits/fe111a60a348b08a66db1037ee99d60f4cb7fade/diff

        :param commitSha: The sha of the commit
        :return: filenames of the sha
        """
        apiResult = requests.get(self.gitlabBaseUrl + "/api/v4/projects/" +
                                 str(self.project_id) + "/repository/commits/" + str(commitSha) + "/diff",
                                 headers=self.headers).json()
        logging.debug("=== AddFilenames call result. sha:  " + str(commitSha) + " isMR: " + "===")
        logging.debug(str(apiResult))
        logging.debug("=== End AddFilenames ===")

        for elem in apiResult:
            logging.debug("elem : " + str(elem))
            filename = elem["new_path"]
            self.MRFilenames[filename] = 1
            self.filerecurrentMRID[filename] = []

    def updateName(self, oldfile, newfile):
        """
        This method updates the file counter when a file gets renamed.
        :param oldfile:
        :param newfile:
        :return:
        """
        if oldfile in self.MRFilenames:
            self.MRFilenames[newfile] = self.MRFilenames[oldfile]
            del self.MRFilenames[oldfile]
            self.filerecurrentMRID[newfile] = self.MRFilenames[oldfile]
            del self.filerecurrentMRID[oldfile]

    def execute(self):

        # Initialize the dictionnary with filenames from the MR.
        logging.info("Calling fetchMRInfo()")
        commitSHA = self.fetchMRInfo()
        if commitSHA:
            self.AddFilenames(commitSHA)

            # Get SHAs X days away
            # shaList = self.getAllSha()
            relevantMRList = self.getRelevantMRs()
            # Foreach SHAs, add filenames
            for rmrid in relevantMRList:
                self.AddMRfiles(rmrid)
                self.FindMaxReccurency()
                self.getMaxFilenameRelevantMRIDs()
            logging.debug(self.MRFilenames)
        else:
            self.MRFilenames = None

    def getRelevantMRs(self):
        """
        Using Days and MRID from config file, get all the relevant future MR and
        return them in a list

        exL https://gitlab.com/api/v4/projects/21860002/merge_requests?state=merged&updated_after=2021-03-01T00:00:00Z&updated_before=2021-05-24T00:00:00Z
        :return:
        """
        RMRList = []

        # Get all MR Merged before X Days
        # Get MR date
        updatedBefore = datetime.strptime(self.MRmergeDate, '%Y-%m-%dT%H:%M:%S.%fZ')
        updatedBefore = updatedBefore + timedelta(days=self.days)
        updatedBefore_string = datetime.strftime(updatedBefore, '%Y-%m-%dT%H:%M:%S.%fZ')

        if self.update_before == "auto":
            self.update_before = updatedBefore_string
        if self.update_after == "auto":
            self.update_after = self.MRmergeDate

        # Get all MR for those dates.
        requestString = self.gitlabBaseUrl + "/api/v4/projects/" + str(self.project_id) + "/merge_requests?state" \
                                                                                          "=merged&updated_after=" + \
                        self.update_after + "&updated_before=" + self.update_before
        logging.info("inside getRelevantMRs : This is the api call : " + requestString)

        apiResult = requests.get(self.gitlabBaseUrl + "/api/v4/projects/" +
                                 str(self.project_id) + "/merge_requests?state=merged&updated_after="
                                 + self.update_after + "&updated_before=" + self.update_before,
                                 headers=self.headers).json()

        # get the first and last 'iid' here
        if apiResult is not None and len(apiResult) > 0:

            if apiResult[-1] is not None:
                first_iid = apiResult[-1]['iid']
            else:
                first_iid = "self"
            if apiResult[0] is not None:
                last_iid = apiResult[0]['iid'] or "self"
            else:
                last_iid = "self"

            self.MRID_range = str(first_iid) + "-" + str(last_iid)
        else:
            self.MRID_range = "self - self"

        logging.debug("*** getRelevantMRs() API result ***")
        logging.debug(str(apiResult))
        logging.debug("*** End getRelevantMRs() ***")
        # Foreach MR, get their Sha
        for mr in apiResult:
            rmrid = mr["iid"]
            RMRList.append(rmrid)

        return RMRList

    def AddMRfiles(self, rmrid):
        """
        This method adds the filenames associated to a mrid to the MRFilename dictionnary
        ex
        https://gitlab.com/api/v4/projects/21860002/merge_requests/68/changes
        GET /projects/:id/merge_requests/:merge_request_iid/changes

        :param mrid: The relevant mrid
        :param isMR: always false = future MRs
        :return: filenames of the sha
        """
        apiResult = requests.get(self.gitlabBaseUrl + "/api/v4/projects/" +
                                 str(self.project_id) + "/merge_requests/" + str(rmrid) + "/changes",
                                 headers=self.headers).json()
        logging.debug("=== AddMRfiles call result. rmrid:  " + str(rmrid) + "===")
        logging.debug(str(apiResult))
        logging.debug("=== End AddMRfiles ===")

        for elem in apiResult["changes"]:
            logging.debug("elem : " + str(elem))
            if elem["renamed_file"] == True:
                self.updateName(elem["old_path"], elem["new_path"])
            filename = elem["new_path"]
            if filename in self.MRFilenames:
                self.MRFilenames[filename] += 1
                self.filerecurrentMRID[filename].append(rmrid)

    def FindMaxReccurency(self):
        maxcount = -1
        maxfilename = ""
        for filename, value in self.MRFilenames.items():
            if int(value) == maxcount:
                maxfilename = maxfilename + " , " + filename
            if int(value) > maxcount:
                maxcount = int(value)
                maxfilename = filename

        self.maxFileName = maxfilename
        self.maxFileCount = maxcount

    def getMaxFilenameRelevantMRIDs(self):
        if self.maxFileName:
            if self.filerecurrentMRID[self.maxFileName]:
                self.maxFileMRList = self.filerecurrentMRID[self.maxFileName]

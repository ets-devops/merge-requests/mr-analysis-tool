import config_reader
import operations

config = config_reader.Reader()
config.config_read()

My_Operation = operations.Operations(config)
dataset = My_Operation.data_reader()
preprocessed_data = My_Operation.data_preprocessor(dataset)
# My_Operation.concatenate_outliers()

My_Operation.correlation_plot(preprocessed_data)
clustered_data, inversed_data = My_Operation.clustering(preprocessed_data, config.InputName)
My_Operation.clustering_plot(clustered_data)

# My_Operation.plot_basic(dataset)
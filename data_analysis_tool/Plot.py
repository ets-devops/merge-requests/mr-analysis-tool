import plotly.graph_objects as go
import matplotlib.pyplot as plt
import seaborn as sns
import random
import numpy as np


class plot:
    def __init__(self, config):
        self.feature_3 = config.feature_3
        self.feature_4 = config.feature_4
        self.feature_5 = config.feature_5
        self.InputName = config.InputName
        self.plot_counter = 0

    def correlation_plot(self, dataframe):
        """
        plots correlation among selected features
        :param dataframe: input dataframe for plotting
        :return:
        """
        correlation_matrix = dataframe.corr()
        heatmap_instance = sns.heatmap(correlation_matrix, linewidths=.5, vmin=-1, vmax=+1, square=True, annot=True)
        heatmap_instance.set_xticklabels(heatmap_instance.get_xticklabels(), rotation=45, horizontalalignment='right')
        return

    def clustering_plot(self, clustered_data):
        """
        Plots clustered data in different colors.
        :param clustered_data:
        :return:
        """
        PLOT = go.Figure()
        for c in list(clustered_data.cluster.unique()):
            PLOT.add_trace(go.Scatter3d(x=clustered_data[clustered_data.cluster == c][self.feature_3],
                                        y=clustered_data[clustered_data.cluster == c][self.feature_4],
                                        z=clustered_data[clustered_data.cluster == c][self.feature_5],
                                        mode='markers', marker_size=3, marker_line_width=1,
                                        name='Cluster ' + str(c))
                           )
        PLOT.update_layout(width=800, height=800, autosize=True, showlegend=True,
                           scene=dict(xaxis=dict(title=self.feature_3, titlefont_color='black'),
                                      yaxis=dict(title=self.feature_4, titlefont_color='black'),
                                      zaxis=dict(title=self.feature_5, titlefont_color='black')),
                           font=dict(family="Gilroy", color='black', size=12))
        PLOT.show()

    def hyperparameter_plot(self, K_range, sum_of_squared_distances, dataframe_name):
        """
        Plots sum of squared distances vs k-means's hyper-parameter(k). Manually choosing k according to
        the illustrated plot is a common way for the hyper-parameter selection
        :param K_range:
        :param sum_of_squared_distances:
        :return:
        """
        r = random.random()
        b = random.random()
        g = random.random()
        color = (r, g, b)

        plt.plot(K_range, sum_of_squared_distances, 'bo--', c=color, linewidth=2, markersize=10, label=dataframe_name)
        plt.xlabel('k')
        plt.ylabel('sum_of_squared_distances')
        plt.title('elbow figure for optimal k')
        plt.grid()
        if self.plot_counter == 1:
            plt.legend(loc="upper left")
            plt.show()
        elif self.plot_counter == 0:
            plt.legend(loc="upper right")
            self.plot_counter += 1
        # plt.plot([K_range[0], K_range[-1]], [sum_of_squared_distances[0], sum_of_squared_distances[-1]], 'ro-')
        return K_range

    def distance_points_line_plot(self, K_range, distance_points_line):
        """
        plots the distance from the line that our innovative method proposes.
        :param K_range:
        :param distance_points_line:
        :return:
        """
        plt.figure(4)
        plt.plot(K_range, distance_points_line, 'bo--', linewidth=2, markersize=10)
        plt.xlabel('k')
        plt.ylabel('distances of the points from the line')
        plt.grid()
        return distance_points_line.index(max(distance_points_line)) + 1

    def silhouette_plot(self, K_range, silhouette_score_list):
        """
        Displays a measure of how close each point in one cluster is to the points in the neighboring clusters and
        thus provides a way to assess parameters like number of clusters visually. :param K_range: :param
        silhouette_score_list: :return:
        """
        K_range = K_range[1:]
        plt.figure(5)
        plt.plot(K_range, silhouette_score_list, color='green', marker='o', linestyle='dashed', linewidth=2,
                 markersize=10)
        plt.xlabel('k')
        plt.ylabel('Silhouette score')
        plt.grid()
        plt.text(silhouette_score_list.index(max(silhouette_score_list)), max(silhouette_score_list),
                 'here is the maximum silhouette score', horizontalalignment='right')
        plt.show()
        return

    def plot_2D_Scatter(self, s, x, y, m, group, all_data):
        x_name = x.name
        y_name = y.name
        s_name = s.name
        m_name = m.name
        g_name = group.name
        colors = ['blue', 'green', 'darkorange', 'red']
        all_data[g_name] = all_data.group.map({"cp": colors[0], "dp": colors[1], "mp": colors[2], "pf": colors[3]})
        s = s.to_list()
        x = x.to_list()
        y = y.to_list()
        m = m.to_list()
        plt.figure(6)
        y_floatlist = [float(value) for value in y]
        y_max = max(y_floatlist)
        plt.text(max(x)-1, y_max, "cp", fontsize=10, color='blue')
        plt.text(max(x)-1, .95*y_max, "dp", fontsize=10, color='green')
        plt.text(max(x)-1, .9*y_max, "mp", fontsize=10, color='darkorange')
        plt.text(max(x)-1, .85*y_max, "pf", fontsize=10, color='red')
        plt.scatter(x, y_floatlist, c=all_data.group, alpha=.4, s=m)
        plt.xticks(np.arange(0, max(x) + 1, 1))
        plt.xlabel(x_name)
        plt.ylabel(y_name)
        plt.suptitle("All Groups")
        plt.title("Scattered Dots' Scale: " + m_name)
        plt.grid(color='r', linestyle='dotted', linewidth=.5)
        plt.show()
        print("----")

    # def plot_2D_Scatter(self, s, x, y, m, all_data):
    #     x_name = x.name
    #     y_name = y.name
    #     s_name = s.name
    #     m_name = m.name
    #     colors = ['blue', 'green', 'darkorange', 'red']
    #     all_data[s_name] = all_data.Sprint.map({1: colors[0], 2: colors[1], 3: colors[2], 4: colors[3]})
    #     s = s.to_list()
    #     x = x.to_list()
    #     y = y.to_list()
    #     m = m.to_list()
    #     plt.figure(6)
    #     y_floatlist = [float(value) for value in y]
    #     y_max = max(y_floatlist)
    #     plt.text(max(x)-1, y_max, "Sprint 1", fontsize=10, color='blue')
    #     plt.text(max(x)-1, .95*y_max, "Sprint 2", fontsize=10, color='green')
    #     plt.text(max(x)-1, .9*y_max, "Sprint 3", fontsize=10, color='darkorange')
    #     plt.text(max(x)-1, .85*y_max, "Sprint 4", fontsize=10, color='red')
    #     plt.scatter(x, y_floatlist, c=all_data.Sprint, alpha=.4, s=m)
    #     plt.xticks(np.arange(0, max(x) + 1, 1))
    #     plt.xlabel(x_name)
    #     plt.ylabel(y_name)
    #     plt.suptitle(self.InputName)
    #     plt.title("Scattered Dots' Scale: " + m_name)
    #     plt.grid(color='r', linestyle='dotted', linewidth=.5)
    #     plt.show()
    #     print("----")

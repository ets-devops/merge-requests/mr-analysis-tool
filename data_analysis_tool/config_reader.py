import json
import ast


class Reader:
    def __init__(self):
        self.feature_0 = None
        self.feature_1 = None
        self.feature_2 = None
        self.feature_3 = None
        self.feature_4 = None
        self.feature_5 = None
        self.feature_6 = None
        self.feature_7 = None
        self.feature_8 = None
        self.features = None
        self.value_metrics = None
        self.file_path = None
        self.clusters_num = None
        self.k_range = None
        self.clusteringEfficiency_Flag = None
        self.outlier_method = None
        self.contamination = None
        self.one_class_nu = None
        self.outlier_methods_ls = None
        self.flag_iforest = None
        self.flag_oneClass = None
        self.replacement_TimeFirstComment = None
        self.replacement_TimeFirstComment_nan = None
        self.replacement_MeanTimeCommits = None
        self.replacement_MeanTimeReview = None
        self.replacement_MeanTimeCommits_nan = None
        self.remove_NAN_flag = None
        self.replace_NumberFinalFiles = None
        self.test_path = None
        self.InputName = None

    def config_read(self):
        """
        this function reads the json configuration file.
        :return: selected metrics and specified number of clusters.
        """
        config_var = "config.json"
        try:
            with open(config_var) as json_data_file:
                config_data = json.load(json_data_file)
                self.file_path = config_data["parameters"]["path"]
                self.InputName = config_data["parameters"]["InputFileName"]
                self.clusters_num = config_data["parameters"]["Number of clusters"]
                self.features = ast.literal_eval(config_data["parameters"]["features"])
                self.value_metrics = ast.literal_eval(config_data["parameters"]["value_metrics"])
                self.k_range = int(config_data["parameters"]["k_range"])
                self.clusteringEfficiency_Flag = config_data["parameters"]["clustering_Efficiency_Flag"]
                self.feature_0 = config_data["features"]["feature_0"]
                self.feature_1 = config_data["features"]["feature_1"]
                self.feature_2 = config_data["features"]["feature_2"]
                self.feature_3 = config_data["features"]["feature_3"]
                self.feature_4 = config_data["features"]["feature_4"]
                self.feature_5 = config_data["features"]["feature_5"]
                self.feature_6 = config_data["features"]["feature_6"]
                self.feature_7 = config_data["features"]["feature_7"]
                self.feature_8 = config_data["features"]["feature_8"]
                self.outlier_method = config_data["parameters"]["outlier_operations"]
                self.contamination = config_data["outliersMethods"]["isolationForest"]["contamination"]
                self.one_class_nu = config_data["outliersMethods"]["oneClassSVM"]["nu"]
                self.outlier_methods_ls = config_data["outliersMethods"].keys()
                self.remove_NAN_flag = config_data["Replacement for dirty metrics"]["Remove NAN values"]
                self.replacement_TimeFirstComment = \
                    str(config_data["Replacement for dirty metrics"]["Time to First Comment"]["No Comment"])
                self.replacement_TimeFirstComment_nan = \
                    str(config_data["Replacement for dirty metrics"]["Time to First Comment"]["nan"])
                self.replacement_MeanTimeCommits = \
                    str(config_data["Replacement for dirty metrics"][" Mean Time between commits"]["no commits"])
                self.replacement_MeanTimeCommits_nan = \
                    str(config_data["Replacement for dirty metrics"][" Mean Time between commits"]["nan"])
                self.replacement_MeanTimeReview = \
                    str(config_data["Replacement for dirty metrics"][" Mean Time between review"]["No Notes"])
                self.replace_NumberFinalFiles = str(
                    config_data["Replacement for dirty metrics"]["# Final Files"]["None"])
                self.test_path = config_data["test"]["path"]

        except Exception as e:
            print("\n Error in setting parameters in the \"config.json\" file")
            file_path = None
            clusters_num = None
            features = None
            return False
        return True

from sklearn.cluster import KMeans
import pandas as pd
import math
from sklearn.metrics import silhouette_score
import numpy as np


class ClusterOperations:
    @staticmethod
    def optimum_k(dataframe, k_range):
        """
         finding the optimum k for k-means clustering using the Elbow method.
        :param dataframe: input dataframe
        :param k_range: k is passed through this function and will be an entry to the 'optimum_k_calc function'.
        :return: sum of squared distances and a list of Silhuoette scores.
        """
        sum_of_squared_distances = []
        silhouette_score_list = []
        K_range = range(1, k_range + 1)
        for k in K_range:
            km_1 = KMeans(n_clusters=k)
            km = km_1.fit(dataframe)
            sum_of_squared_distances.append(km.inertia_)
            if k > 1:
                score = silhouette_score(dataframe, km_1.labels_)
                silhouette_score_list.append(score)
        return sum_of_squared_distances, K_range, silhouette_score_list

    def optimum_K_calc(self, sum_of_squared_distances, K_range):
        """
        This is an innovative method used as an alternative to the Elbow method for the automatic retrieval of tuned
        hyper-parameter. This method interacts with "calculate_distance function" to calculate distance between the
        line which connects first point in the sum of squared distance plot to its last point.
        :param
        sum_of_squared_distances: sum of distances from each data point in a cluster to their corresponding cluster
        centroids. :param K_range: specified k with which iterations are done to obtain the optimum hyper-parameter.
        :return: A list of the distance point line.
        """
        a = sum_of_squared_distances[0] - sum_of_squared_distances[-1]
        b = K_range[-1] - K_range[0]
        c1 = K_range[0] * sum_of_squared_distances[-1]
        c2 = K_range[-1] * sum_of_squared_distances[0]
        c = c1 - c2
        distance_points_line = []
        for k in K_range:
            distance_points_line.append(
                self.calculate_distance(K_range[k - 1], sum_of_squared_distances[k - 1], a, b, c))
        return distance_points_line

    @staticmethod
    def calculate_distance(x1, y1, a, b, c):
        """
        Distance between the points and line is calculated.
        :param x1: :param y1: :param a: :param b: :param c: required parameters to draw a line.
        :return: distance between the points and line.
        """
        distance = abs((a * x1 + b * y1 + c)) / (math.sqrt(a * a + b * b))
        return distance

    def k_means(self, normalized_data, optimum_k, value_metrics):
        """
        k-means clustering calculations and adding the cluster's number column to our dataset.
        :param normalized_data: This parameter will be passed through this function.
        :param optimum_k: The best k as the result of hp tuning.
        :return: normalized centroids and a copy of normalized data.
        """
        kmeans = KMeans(n_clusters=int(optimum_k))
        new_normlized_data = normalized_data.copy()
        kmeans.fit(new_normlized_data[value_metrics])
        new_normlized_data.loc[:, 'cluster'] = kmeans.labels_
        centroids = kmeans.cluster_centers_
        return new_normlized_data, centroids

    def inverse_function(self, primary_dataset, centroids, selected_features):
        """
        Returns the real values of clusters from the normalized domain.
        :param primary_dataset:
        :param centroids:
        :param selected_features:
        :return:
        """
        centroids = centroids.to_numpy()
        j = 0
        while j < len(selected_features):
            for s in selected_features:
                a = [float(value) for value in list(primary_dataset[s])]
                maximum = max(a)
                minimum = min(a)
                i = 0
                while i < len(centroids):
                    centroids[i][j] = int((centroids[i][j] * (maximum - minimum)) + minimum)
                    i += 1
                j += 1
        centroids = pd.DataFrame(centroids)
        centroids.columns = selected_features
        return centroids

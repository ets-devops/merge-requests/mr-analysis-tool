import pandas as pd
from sklearn.preprocessing import MinMaxScaler
from scipy.stats import zscore
import numpy as np
from sklearn.ensemble import IsolationForest
from sklearn.svm import OneClassSVM


class Preprocess:

    def __init__(self, features):
        self.data_preprocess = pd.DataFrame()
        self.scaler = MinMaxScaler()
        self.variable_strings = {}
        self.features = features
        self.list_dirty_metrics = ["MR Size", "Lead Time", "Mean Time between commits", "Time to First Comment"]
        # for feature in self.features:
        #     str_name = "str_%s" % (str(feature).replace(" ", "").replace("#", "number_of_"))
        #     # vars()[str_name] = feature
        #     self.variable_strings[str_name] = feature

    def data_cleaning(self, dataset, remove_NAN_Flag, replace_TimeFirstCommits, replace_MeanTimeComment,
                      numberFinalFiles, replace_MeanTimeReview, replace_MeanTimeReview_nan,
                      replace_TimeFirstCommits_nan):
        """
        Replacing NAN values with zero values in input feature. What's more method removes opened MRs, Mean Time between
        commits where cells have no commits.
        :param dataset:
        :return:
        """
        dataset = dataset[dataset["State"].str.contains("opened") == False]
        dataset = dataset.fillna(" ")
        if str(remove_NAN_Flag).lower() == "true":
            dataset = dataset[dataset[" Mean Time between commits"].str.contains("no commits") == False]
            dataset = dataset[dataset["Time to First Comment"].str.contains("No Comment") == False]
            dataset = dataset[dataset["Mean Time between review"].str.contains("No Notes") == False]
            dataset = dataset[dataset["Mean Time between review"].str.contains("nan") == False]
            dataset = dataset[dataset["Time ti First Comment"].str.contains("nan") == False]

            # dataset = dataset[dataset["# Final Files"].str.contains("None") == False]
        else:
            dataset["Time to First Comment"].replace({"No Comment": replace_TimeFirstCommits}, inplace=True)
            dataset[" Mean Time between commits"].replace({"no commits": replace_MeanTimeComment}, inplace=True)
            dataset["Mean Time between review"].replace({"No Notes": replace_MeanTimeReview}, inplace=True)
            dataset["Mean Time between review"].replace({"nan": replace_MeanTimeReview_nan}, inplace=True)
            dataset["Time to First Comment"].replace({" ": replace_TimeFirstCommits_nan}, inplace=True)
            dataset["# Final Files"].replace({"None": numberFinalFiles}, inplace=True)

        cleaned_dataframe = pd.DataFrame(dataset)
        return cleaned_dataframe

    def isolation_forest(self, dataframe, contamination):
        """
        Removing outliers .
        :param dataframe:
        :return:
        """
        fitter = IsolationForest(contamination=0.1)

        yhat = fitter.fit_predict(dataframe)
        mask = yhat != -1
        filtered_dataframe = dataframe[mask]

        mask = yhat != 1
        outliers = dataframe[mask]

        return filtered_dataframe, outliers

    def oneClass_SVM(self, dataframe, nu):
        """
        :param dataframe:
        :param nu:
        :return:
        """
        fitter = OneClassSVM(nu=0.01)
        yhat = fitter.fit_predict(dataframe)
        mask = yhat != -1
        filtered_dataframe = dataframe[mask]
        mask = yhat != 1
        outliers = dataframe[mask]
        return filtered_dataframe, outliers

    def z_score(self, dataframe):
        """
        :param dataframe:
        :return:
        """
        z_scores = zscore(dataframe)
        abs_z_scores = np.abs(z_scores)
        filtered_entries = (abs_z_scores < 3).all(axis=1)
        filtered_dataframe = dataframe[filtered_entries]
        return filtered_dataframe, filtered_entries

    def getFeature(self, dataset, features):
        """
        Opting out desired features.
        :param dataset:
        :param features:
        :return:
        """
        final_dataset = dataset[features]
        return final_dataset

    def data_normalization(self, dataset, features):
        """
        Normalizing through scaling method.
        :param dataset:
        :param features:
        :return:
        """
        return self.scaler.fit_transform(dataset[features])

    def inversion(self, normalized_data):
        """
        Converting normalized data into their real values.
        :param normalized_data:
        :return:
        """
        return pd.DataFrame(self.scaler.inverse_transform(
            normalized_data))

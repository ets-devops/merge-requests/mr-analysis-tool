import preprocessing
import clustering
import provision
import Plot
from config_reader import Reader
import pandas as pd
import provision


class Operations:

    def __init__(self, config: Reader):
        self.final_dataset = []
        self.prepro = None
        self.cluster = clustering.ClusterOperations()
        self.provisioner = provision.provision()
        self.config = config
        self.plotter = None
        self.prepro = preprocessing.Preprocess(self.config.features)
        self.plotter = Plot.plot(config=self.config)
        self.config.outlier_flag = False
        self.dataframe_iforest = None
        self.dataframe_oneClassSVM = None

    def data_reader(self):
        """
        simply reads dataset
        :return: dataset
        """
        if (self.config.file_path or self.config.clusters_num or self.config.features) is not None:
            dataset = pd.read_csv(self.config.file_path)
            dataset = dataset[dataset["State"].str.contains("opened") == False]
            return dataset
        else:
            print("config file parameters are not correct")

    def data_preprocessor(self, dataset):
        clean_data = self.prepro.data_cleaning(dataset, self.config.remove_NAN_flag,
                                               self.config.replacement_MeanTimeCommits,
                                               self.config.replacement_TimeFirstComment,
                                               self.config.replace_NumberFinalFiles,
                                               self.config.replacement_MeanTimeReview,
                                               self.config.replacement_MeanTimeCommits_nan,
                                               self.config.replacement_TimeFirstComment_nan)
        clean_data = clean_data[self.config.features]

        normalized_data = clean_data[self.config.features]

        primary_dataset = self.prepro.data_cleaning(pd.read_csv(self.config.file_path), self.config.remove_NAN_flag,
                                                    self.config.replacement_MeanTimeCommits,
                                                    self.config.replacement_TimeFirstComment,
                                                    self.config.replace_NumberFinalFiles,
                                                    self.config.replacement_MeanTimeReview,
                                                    self.config.replacement_MeanTimeCommits_nan,
                                                    self.config.replacement_TimeFirstComment_nan)
        self.final_dataset = primary_dataset[self.config.features]
        normalized_data[self.config.value_metrics] = self.prepro.data_normalization(
            self.final_dataset[self.config.value_metrics],
            self.config.value_metrics)

        for outlier_method in self.config.outlier_method:
            if outlier_method == "isolation_forest":
                if self.config.outlier_flag:
                    print("error! Enter only one valid outlier method")
                else:
                    self.config.outlier_flag = True
            if outlier_method == "one_class_SVM":
                if self.config.outlier_flag:
                    print("error! Enter only one valid outlier method")
                else:
                    self.config.outlier_flag = True

        for outlier_method in self.config.outlier_methods_ls:
            if outlier_method == "isolationForest":
                self.dataframe_iforest, outliers_iforest = self.prepro.isolation_forest(dataframe=normalized_data,
                                                                                        contamination=self.config.contamination)
            if outlier_method == "oneClassSVM":
                self.dataframe_oneClassSVM, outliers_oneClassSVM = self.prepro.oneClass_SVM(dataframe=normalized_data,
                                                                                            nu=self.config.one_class_nu)

        self.provisioner.csv_maker(csv_name=".\\Outliers\\outliers-" + self.config.InputName + ".csv",
                                   input_df=outliers_iforest)
        return self.dataframe_iforest

    def concatenate_outliers(self):
        df = pd.DataFrame(list())
        try:
            if "-cp" in self.config.file_path:
                df.to_csv(".\\Outliers\\outliers-cp.csv", index=False, header=False)
                self.provisioner.concatenate_csvs(sample_new=".\\Outliers\\outliers-cp.csv",
                                                  sample1=".\\Outliers\\outliers-2020-cp.csv",
                                                  sample2=".\\Outliers\\outliers-2021-cp.csv")
            if "-dp" in self.config.file_path:
                df.to_csv(".\\Outliers\\outliers-dp.csv", index=False, header=False)
                self.provisioner.concatenate_csvs(sample_new=".\\Outliers\\outliers-dp.csv",
                                                  sample1=".\\Outliers\\outliers-2020-dp.csv",
                                                  sample2=".\\Outliers\\outliers-2021-dp.csv")
            if "-mp" in self.config.file_path:
                df.to_csv(".\\Outliers\\outliers-mp.csv", index=False, header=False)
                self.provisioner.concatenate_csvs(sample_new=".\\Outliers\\outliers-mp.csv",
                                                  sample1=".\\Outliers\\outliers-2020-mp.csv",
                                                  sample2=".\\Outliers\\outliers-2021-mp.csv")
            if "-pf" in self.config.file_path:
                df.to_csv(".\\Outliers\\outliers-pf.csv", index=False, header=False)
                self.provisioner.concatenate_csvs(sample_new=".\\Outliers\\outliers-pf.csv",
                                                  sample1=".\\Outliers\\outliers-2020-pf.csv",
                                                  sample2=".\\Outliers\\outliers-2021-pf.csv")
        except:
            print("Error in concatenating csv files")

    def correlation_plot(self, dataset):
        self.plotter.correlation_plot(dataset[self.config.value_metrics])

    def clustering(self, new_dataframe, fileInputName):
        """
        clusters data frames based on the optimum k obtained.
        """
        # clustering
        if (not (self.config.clusters_num.strip())) or \
                (str(self.config.clusters_num.strip()).lower() == "auto") or \
                (type(int(self.config.clusters_num)) != int):

            sum_of_squared_distances, K_range, silhouette_score_list = self.cluster.optimum_k(
                new_dataframe[self.config.features], self.config.k_range)
            K_range = self.plotter.hyperparameter_plot(K_range, sum_of_squared_distances, fileInputName)
            distance_points_line = self.cluster.optimum_K_calc(sum_of_squared_distances, K_range)
            optimum_k = self.plotter.distance_points_line_plot(K_range, distance_points_line)
            # self.plotter.silhouette_plot(K_range, silhouette_score_list)

        elif type(int(self.config.clusters_num)) == int:
            optimum_k = self.config.clusters_num

        clustered_data, centroids = self.cluster.k_means(new_dataframe, optimum_k, self.config.value_metrics)
        centroids = pd.DataFrame(centroids)
        centroids.columns = self.config.value_metrics
        # Inversion
        real_centroids = self.cluster.inverse_function(self.final_dataset[self.config.value_metrics],
                                                       centroids[self.config.value_metrics],
                                                       self.config.value_metrics)
        clustered_sorted = self.provisioner.data_sorter(dataset=clustered_data, sort_base="cluster")

        # real_centroids.columns = config.features
        inverse_data = self.prepro.inversion(clustered_sorted[self.config.value_metrics])
        inverse_data.columns = self.config.value_metrics
        inverse_data["cluster"] = clustered_sorted["cluster"].values
        inverse_data["Project ID"] = clustered_sorted["Project ID"].values
        inverse_data["MR ID"] = clustered_sorted["MR ID"].values
        inverse_data["Sprint"] = clustered_sorted["Sprint"].values
        inverse_data["Sprint Day"] = clustered_sorted["Sprint Day"].values

        self.provisioner.csv_maker(input_df=real_centroids, csv_name="Centroids_" + self.config.InputName + ".csv")
        self.provisioner.csv_maker(input_df=inverse_data, csv_name="clustered_data_" + self.config.InputName + ".csv")

        return clustered_data, inverse_data

    def clustering_plot(self, clustered_data):
        self.plotter.clustering_plot(clustered_data)

    def plot_basic(self, clustered_inversed):
        # "features": {
        #     "feature_0": "MR ID",
        #     "feature_1": "Sprint",
        #     "feature_2": "Sprint Day",
        #     "feature_3": "Lead Time",
        #     "feature_4": "MR Size",
        #     "feature_5": "Mean Time between review",
        #     "feature_6": " Mean Time between commits",
        #     "feature_7": "Creation Date"

        self.plotter.plot_2D_Scatter(s=clustered_inversed[self.config.feature_1],
                                     x=clustered_inversed[self.config.feature_2],
                                     y=clustered_inversed[self.config.feature_3],
                                     m=clustered_inversed[self.config.feature_4],
                                     group=clustered_inversed[self.config.feature_8],
                                     all_data=clustered_inversed)

    print("Done")

import unittest
import json
import pandas as pd
import os

import preprocessing
import config_reader
import operations
import provision


class test_methods(unittest.TestCase):
    def __init__(self, config):
        self.actual = None
        self.expected = None
        self.config = config
        self.preprocessor = preprocessing.Preprocess(self.config.features)
        self.myOperations = operations.Operations(self.config)
        self.provisioning = provision.provision()

    def test_cleaned_df(self):
        self.expected = pd.read_csv(self.config.test_path).to_csv('cleaned_expected.csv', index=False,
                                                                  float_format='%.0f')

        self.actual = self.preprocessor.data_cleaning(pd.read_csv(self.config.file_path), self.config.remove_NAN_flag,
                                                      self.config.replacement_MeanTimeCommits,
                                                      self.config.replacement_TimeFirstComment,
                                                      self.config.replace_NumberFinalFiles)

        self.actual.to_csv('cleaned_actual.csv', index=False)
        pd.read_csv('cleaned_actual.csv').to_csv('cleaned_actual_new.csv', index=False, float_format='%.0f')
        with open('cleaned_expected.csv', 'r') as t1, open('cleaned_actual_new.csv', 'r') as t2:
            fileone = t1.readlines()
            filetwo = t2.readlines()

        with open('update.csv', 'w') as outFile:
            for line in filetwo:
                if line not in fileone:
                    outFile.write(line)

        if os.stat("update.csv").st_size == 0:
            print("Cleaning Test --> Ok")
        else:
            print("Cleaning Test --> Failed")

        # if pd.testing.assert_frame_equal(self.actual.sort_values(by=self.config.feature_7).reset_index(drop=True),
        #                                  self.expected.sort_values(by=self.config.feature_7).reset_index(
        #                                      drop=True)) is None:
        #     print("Ok")

        # if self.actual.shape == self.expected.shape:
        #     print("Ok")

    def efficiency_outlier_methods(self):
        data_frame = pd.read_csv(self.config.file_path)
        filtered_dataframe_iforest, outliers_iforest = self.preprocessor.isolation_forest(
            self.actual[self.config.value_metrics],
            self.config.contamination)
        filtered_dataframe_oneClassSVM, outliers_oneClassSVM = self.preprocessor.oneClass_SVM(
            self.actual[self.config.value_metrics],
            self.config.contamination)

        diff_outliers = pd.concat([outliers_iforest, outliers_oneClassSVM]).drop_duplicates(
            keep=False)
        return diff_outliers

    def clustering_efficiency(self):
        self.myOperations.data_preprocessor(self.myOperations.data_reader())
        clustered_oneClass = self.myOperations.clustering(new_dataframe=self.myOperations.dataframe_oneClassSVM,
                                                          fileInputName="oneClassSVM")
        clustered_iforest = self.myOperations.clustering(new_dataframe=self.myOperations.dataframe_iforest,
                                                         fileInputName="iForest")
        print("Plot drawn")

    def concatenate_first(self):
        self.provisioning.concatenate_csvs(
            sample_new="C:\\Users\\SeyedbehnamMashari\\Desktop\\sample_CSV\\2020-2021-06-21\\Concatenated_first\\2020-cp-concatenated.csv",
            sample1="C:\\Users\\SeyedbehnamMashari\\Desktop\\sample_CSV\\2020-2021-06-21\\2020-cp\\results.csv",
            sample2="C:\\Users\\SeyedbehnamMashari\\Desktop\\sample_CSV\\2020-2021-06-21\\2021-cp\\results.csv")
        self.provisioning.concatenate_csvs(
            sample_new="C:\\Users\\SeyedbehnamMashari\\Desktop\\sample_CSV\\2020-2021-06-21\\Concatenated_first\\2020-dp-concatenated.csv",
            sample1="C:\\Users\\SeyedbehnamMashari\\Desktop\\sample_CSV\\2020-2021-06-21\\2020-dp\\results.csv",
            sample2="C:\\Users\\SeyedbehnamMashari\\Desktop\\sample_CSV\\2020-2021-06-21\\2021-dp\\results.csv")
        self.provisioning.concatenate_csvs(
            sample_new="C:\\Users\\SeyedbehnamMashari\\Desktop\\sample_CSV\\2020-2021-06-21\\Concatenated_first\\2020-mp-concatenated.csv",
            sample1="C:\\Users\\SeyedbehnamMashari\\Desktop\\sample_CSV\\2020-2021-06-21\\2020-mp\\results.csv",
            sample2="C:\\Users\\SeyedbehnamMashari\\Desktop\\sample_CSV\\2020-2021-06-21\\2021-mp\\results.csv")
        self.provisioning.concatenate_csvs(
            sample_new="C:\\Users\\SeyedbehnamMashari\\Desktop\\sample_CSV\\2020-2021-06-21\\Concatenated_first\\2020-pf-concatenated.csv",
            sample1="C:\\Users\\SeyedbehnamMashari\\Desktop\\sample_CSV\\2020-2021-06-21\\2020-pf\\results.csv",
            sample2="C:\\Users\\SeyedbehnamMashari\\Desktop\\sample_CSV\\2020-2021-06-21\\2021-pf\\results.csv")

    def concatenate_all_groups(self):
        self.provisioning.concatenate_all(
            sample_new="C:\\Users\\SeyedbehnamMashari\\Desktop\\sample_CSV\\2020-2021-06-21\\Concatenated_first\\Concat_group_names\\all_groups-concatenated.csv",
            sample1="C:\\Users\\SeyedbehnamMashari\\Desktop\\sample_CSV\\2020-2021-06-21\\Concatenated_first\\Concat_group_names\\cp.csv",
            sample2="C:\\Users\\SeyedbehnamMashari\\Desktop\\sample_CSV\\2020-2021-06-21\\Concatenated_first\\Concat_group_names\\dp.csv",
            sample3="C:\\Users\\SeyedbehnamMashari\\Desktop\\sample_CSV\\2020-2021-06-21\\Concatenated_first\\Concat_group_names\\mp.csv",
            sample4="C:\\Users\\SeyedbehnamMashari\\Desktop\\sample_CSV\\2020-2021-06-21\\Concatenated_first\\Concat_group_names\\pf.csv")

    def add_Group_Column(self):
        self.provisioning.add_column_group(
            sample_csv_path="C:\\Users\\SeyedbehnamMashari\\Desktop\\sample_CSV\\2020-2021-06-21\\Concatenated_first\\2020-cp-concatenated.csv",
            group_name="cp")
        self.provisioning.add_column_group(
            sample_csv_path="C:\\Users\\SeyedbehnamMashari\\Desktop\\sample_CSV\\2020-2021-06-21\\Concatenated_first\\2020-dp-concatenated.csv",
            group_name="dp")
        self.provisioning.add_column_group(
            sample_csv_path="C:\\Users\\SeyedbehnamMashari\\Desktop\\sample_CSV\\2020-2021-06-21\\Concatenated_first\\2020-mp-concatenated.csv",
            group_name="mp")
        self.provisioning.add_column_group(
            sample_csv_path="C:\\Users\\SeyedbehnamMashari\\Desktop\\sample_CSV\\2020-2021-06-21\\Concatenated_first\\2020-pf-concatenated.csv",
            group_name="pf")


config = config_reader.Reader()
config.config_read()
test_instance = test_methods(config)

# test_instance.test_cleaned_df()

# if str(config.clusteringEfficiency_Flag).lower() == "true":
#     test_instance.clustering_efficiency()
# test_instance.concatenate_first()
# test_instance.add_Group_Column()
test_instance.concatenate_all_groups()

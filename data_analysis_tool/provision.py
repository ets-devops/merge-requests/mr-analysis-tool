import pandas as pd
import config_reader


class provision:
    def data_sorter(self, dataset, sort_base):
        """
        Sorting values according to the defined column header (feature).
        :param dataset:
        :param sort_base:
        :return:
        """
        return dataset.sort_values(by=sort_base)

    def appender(self, data_only, headers):
        """
        Appends data the selected headers.
        :param data_only:
        :param headers:
        :return:
        """
        return pd.DataFrame(headers).append(data_only)

    def csv_maker(self, input_df, csv_name):
        """
        creates csv file format.
        :param input_df:
        :param csv_name:
        :return:
        """
        return input_df.to_csv(csv_name, index=False)

    def concatenate_csvs(self, sample_new, sample1, sample2):
        file0 = open(sample_new, "a")
        file1 = open(sample1, "r")
        file2 = open(sample2, "r")
        counter = 0
        for line in file1:
            file0.write(line)
        for line in file2:
            if counter == 1:
                file0.write(line)
            else:
                counter += 1

        file0.close()
        file1.close()
        file2.close()

    def concatenate_all(self, sample_new, sample1, sample2, sample3, sample4):
        file0 = open(sample_new, "a")
        file1 = open(sample1, "r")
        file2 = open(sample2, "r")
        file3 = open(sample3, "r")
        file4 = open(sample4, "r")
        counter = 0
        for line in file1:
            file0.write(line)
        for line in file2:
            if counter == 1:
                file0.write(line)
            else:
                counter += 1
        counter = 0
        for line in file3:
            if counter == 1:
                file0.write(line)
            else:
                counter += 1
        counter = 0
        for line in file4:
            if counter == 1:
                file0.write(line)
            else:
                counter += 1
        file0.close()
        file1.close()
        file2.close()
        file3.close()
        file4.close()

    def add_column_group(self, sample_csv_path, group_name):
        sample_csv = pd.read_csv(sample_csv_path, index_col=False)
        pd.DataFrame(sample_csv)["group"] = group_name
        pd.DataFrame(sample_csv).to_csv(
            "C:\\Users\\SeyedbehnamMashari\\Desktop\\sample_CSV\\2020-2021-06-21\\Concatenated_first\\Concat_group_names\\" + group_name + ".csv",
            index=None)
